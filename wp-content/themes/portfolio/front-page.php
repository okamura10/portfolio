<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>portfolio</title>
  <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
  <!-- Noto Sans JPのフォントリンク -->
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
</head>
<body>

  <section id="main" class="main">
    <div class="title">
      <h1>
        <img class="img" src="<?php echo get_template_directory_uri(); ?>/assets/images/title.png" alt="A to Z" width="197">
      </h1>
      <p>「<span class="sub-title">これまで</span>」と「<span class="sub-title">これから</span>」</p>
    </div>
    <div id="scroll" class="arrow scroll">
      <p>SCROLL</p>
      <svg class="arrow-1">
        <path fill-rule="evenodd"  stroke="rgb(255, 255, 255)" stroke-width="5px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M40.493,7.084 L21.491,24.316 L2.489,7.084 "/>
      </svg>
      <svg class="arrow-2">
        <path fill-rule="evenodd"  stroke="rgb(255, 255, 255)" stroke-width="5px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M40.493,7.084 L21.491,24.316 L2.489,7.084 "/>
      </svg>
    </div>
  </section>

  <section class="profile">
    <div class="profile__wrap">
      <h2>プロフィール</h2>
      <div class="align_center">
        <img class="img" src="<?php echo get_template_directory_uri(); ?>/assets/images/top_profile.png" alt="プロフィール写真">
        <p class="name">TAKERU OKAMURA</p>
        <p class="birthday">1999/10/27</p>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="content__wrap">
      <h2>制作作品</h2>
      <ul class="inner">
        <?php
          $args = array(
          'posts_per_page' => 9 // 表示件数の指定
          );
          $posts = get_posts( $args );
          foreach ( $posts as $post ): // ループの開始
          setup_postdata( $post ); // 記事データの取得
        ?>
        <li class="col">
          <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail( $size, $attr )?>
            <p class="title"><?php the_title(); ?></p>
          </a>
          <p class="category">
            <?php $cat = get_the_category(); ?>
            <?php $cat = $cat[0]; ?>
            <?php echo get_cat_name($cat->term_id); ?>
          </p>
        </li>
        <?php
          endforeach; // ループの終了
          wp_reset_postdata(); // 直前のクエリを復元する
        ?>
      </ul>
    </div>
  </section>

  <section class="adress">
    <div class="adress__wrap">
      <h2>Adress</h2>
      <div class="inner">
        <ul class="link-1">
          <li><a class="facebook" href="https://www.facebook.com/1999takepicture">FaceBook</a></li>
          <li><a class="codepen" href="https://codepen.io/okamura10/">CodePen</a></li>
        </ul>
        <h3>gitリポジトリ</h3>
        <ul class="link-2">
          <li><a href="https://bitbucket.org/okamura10/portfolio/src/master/">portfolioサイト<small>現サイト</small></a></li>
          <li><a href="https://bitbucket.org/okamura10/wordpress_sample/src/master/">WordPress環境サンプル</a></li>
        </ul>
      </div>
    </div>
  </section>

  <?php get_footer(); ?>

  <script src="<?php bloginfo('template_url'); ?>/assets/js/common.js"></script>
</body>
</html>