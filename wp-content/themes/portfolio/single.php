<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>portfolio</title>
  <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
  <!-- Noto Sans JPのフォントリンク -->
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
</head>

<?php if(have_posts()): while(have_posts()):the_post(); ?>
  <section class="single">
    <h1><?php the_title();?></h1>
    <div class="inner">
      <?php the_post_thumbnail( $size, $attr )?>
      <div class="txt">
        <p class="about">概要</p>
        <div class="read">
          <?php 
            $content = get_the_content();
            $content = strip_tags($content);
            echo $content; 
          ?>
        </div>
      </div>
    </div>
    <p class="time"></p>
    <img class="img" src="<?php echo catch_that_image(); ?>" alt="<?php the_title(); ?>" />
    <p class="back-home">
      <?php
          echo '<a href="' . $_SERVER['HTTP_REFERER'] . '">HOMEに戻る</a>';
      ?></p>
  </section>
<?php endwhile; endif; ?>
  