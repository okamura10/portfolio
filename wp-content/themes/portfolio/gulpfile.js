//プラグインの読み込み
const gulp = require('gulp');
const sass = require('gulp-sass');
const watch = require('gulp-watch');


//Sassのコンパイル
gulp.task('sass', () => {
  return gulp.src('assets/scss/style.scss')
    .pipe(sass({
      outputStyle: 'compact' //通常の見た目にしたい時はexpanded
    }))
    .pipe(gulp.dest('./'));
});

//ファイルの監視をする
gulp.task('watch', () => {
  gulp.watch('assets/scss/**/*.scss', gulp.series(['sass']));
});

//デフォルトに設定
gulp.task('default', gulp.series(['watch'], () => {
}));