//mainのbgの高さ指定
var c1 = document.getElementById('main'); //c1でuIDの取得

window.addEventListener('load', function(){ //ロード時にデータ取得
  var h = window.innerHeight; //ウィンドウの表示の高さを取得 hに代入
  var style = c1.style; //変数styleにmainのstyleを指定
  style.minHeight = h + 'px'; //minheightに値を指定
}, false);

window.addEventListener('resize', function(){
  var h = window.innerHeight;
  var style = c1.style;
  style.minHeight = h + 'px';
}, false);


//.scroll部分のscrollanimation
document.getElementById("scroll").onclick = function() {
  // ここに#buttonをクリックしたら発生させる処理を記述する
  var h = window.innerHeight;
  window.scrollTo(0,h);
};