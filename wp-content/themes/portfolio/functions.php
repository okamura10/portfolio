<?php
/**
 * sample functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage sample
 * @since 1.0
 */
 
 // titleタグをhead内に生成する
add_theme_support( 'title-tag' );
// HTML5でマークアップさせる
add_theme_support( 'html5', array(
  'comment-form',
  'comment-list',
  'gallery',
  'caption',
));
// Feedのリンクを自動で生成する
add_theme_support( 'automatic-feed-links' );
//アイキャッチ画像を使用する設定
add_theme_support( 'post-thumbnails' );

function catch_that_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

if(empty($first_img)){ //Defines a default image
      $first_img = "/images/default.jpg";
  }
  return $first_img;
} 

$content = strip_tags(strip_shortcodes(get_the_content('')));

?>
