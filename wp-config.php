<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'olljwny4wPjzj6jJRxPTPsAauKzyp2DkDNp761hInLojWFr0kZ2P9hvLAbB0JTOc4jD/uSqbXq0VKGjVazc3Xw==');
define('SECURE_AUTH_KEY',  'eTSe28kj4sGGr+ePJkhVZoReTNVD2YKCD7XDf9bqJ5CyFxnaRMhARBdhVW6ZOSYQ0VzF6FgLstgdcS+CjHY3lA==');
define('LOGGED_IN_KEY',    'nRdVM2wGRd1VozDjB6z/m+OJJ3XHbLsG5lkz42s9NP7kf6dRNueNJt2h3OicJacDNoaJlBhwEuWu2G8O5WS4sQ==');
define('NONCE_KEY',        'bzXBVLlqQhiuwMzPsGJBXyh6NjIxpWVaAZ93+8xZ+o7iFJcBaA5zxsNB51FGz1LCr684Q2PGLsDy7OzsPk3NXw==');
define('AUTH_SALT',        '/VQr3wSs4Hw4BIhcnfFOCg10QE2x4xGA8C2feIIzfZsk+5fYN9weMlISyUr8cW+z6suR6olsaj3KijNo0eHcSg==');
define('SECURE_AUTH_SALT', 'HJfeeUMJCl4tt78tZ0h/d1I8+KSTWFRiId65kYzAqHIubu9C8mBTuviS2S5jLl7TBqXKgWYEQL9Pu9Q+im0skA==');
define('LOGGED_IN_SALT',   'YfnY8uIe+CNbl6rLl2BrB8FYx3RRaE27CshI+H8tPPEw4J21ZBkQdFpQtoMVCQXrN+xJmOj/Je44o4oT5GFFvw==');
define('NONCE_SALT',       'aQQPdqQbZ2zr3lmsapD3SlWTFVm1cpAqA37SzgCzEV07fWa6Q/ZT6CRPabFyJjxpcbXyk6kSKyJSEFAfUTp/nw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
